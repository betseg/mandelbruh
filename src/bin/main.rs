use mandelbruh::Mandelbrot;

fn main() {
    // very big
    // let m = Mandelbrot::new()
    //     .topleft(1.2, -2.)
    //     .width(3.)
    //     .height(2.4)
    //     .scale(50.);
    // println!("{}", m);

    // zoomed
    // let m = Mandelbrot::new()
    //     .topleft(0.72, -0.34)
    //     .width(0.15)
    //     .height(0.1)
    //     .scale(1500.);
    // println!("{}", m);

    // default
    let m = Mandelbrot::new();
    println!("{}", m);
}
