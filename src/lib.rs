use ansi_term::{
    Colour,
    Colour::{Black, RGB},
    Style,
};
use itertools::{
    FoldWhile::{Continue, Done},
    Itertools,
};
use num_complex::Complex64;
use palette::{Hsv, Pixel, Srgb};
use std::fmt;

#[derive(Debug)]
pub struct Mandelbrot {
    xmin: f64,
    xmax: f64,
    ymin: f64,
    ymax: f64,
    scale: f64,
}

impl Default for Mandelbrot {
    fn default() -> Self {
        Self {
            xmin: -2.,
            xmax: 1.,
            ymin: -1.2,
            ymax: 1.2,
            scale: 10.,
        }
    }
}

impl Mandelbrot {
    #[inline]
    pub fn new() -> Self {
        Default::default()
    }

    #[inline]
    pub fn topleft(self, top: f64, left: f64) -> Self {
        Self {
            xmin: left,
            ymax: top,
            ..self
        }
    }

    #[inline]
    pub fn width(self, width: f64) -> Self {
        Self {
            xmax: self.xmin + width,
            ..self
        }
    }

    #[inline]
    pub fn height(self, height: f64) -> Self {
        Self {
            ymin: self.ymax - height,
            ..self
        }
    }

    #[inline]
    pub fn scale(self, scale: f64) -> Self {
        Self { scale, ..self }
    }

    #[inline]
    fn iter(c: Complex64) -> usize {
        let fc = |z: Complex64, c| z * z + c;
        (0..=37)
            .fold_while((Complex64::new(0., 0.), 0), |(acc, _), n| {
                if acc.norm() > 2. {
                    Done((Default::default(), n))
                } else {
                    Continue((fc(acc, c), n))
                }
            })
            .into_inner()
            .1
            - 1
    }

    #[inline]
    fn get_colour(i: usize) -> Colour {
        if i == 36 {
            Black
        } else {
            let colour: [u8; 3] = Srgb::from(Hsv::new(i as f64 * 10., 1., 1.))
                .into_linear()
                .into_format()
                .into_raw();
            RGB(colour[0], colour[1], colour[2])
        }
    }
}

impl fmt::Display for Mandelbrot {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        for y in (((self.ymin * self.scale) as i64)..=((self.ymax * self.scale) as i64)).rev() {
            for x in ((self.xmin * self.scale) as i64)..((self.xmax * self.scale) as i64) {
                write!(
                    f,
                    "{}",
                    Style::new()
                        .on(Self::get_colour(Self::iter(
                            Complex64::new(x as f64, y as f64) / self.scale
                        )))
                        .paint("  ")
                )?;
            }
            writeln!(f)?;
        }
        Ok(())
    }
}
